package se331.lab.rest.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import se331.lab.rest.entity.Course;
import se331.lab.rest.entity.Lecturer;
import se331.lab.rest.entity.Student;
import se331.lab.rest.repository.CourseRepository;
import se331.lab.rest.repository.LecturerRepository;
import se331.lab.rest.repository.StudentRepository;

import javax.transaction.Transactional;

@Component
public class DataLoader implements ApplicationRunner {
    @Autowired
    StudentRepository studentRepository;
    @Autowired
    LecturerRepository lecturerRepository;
    @Autowired
    CourseRepository courseRepository;
    @Transactional
    @Override
    public void run(ApplicationArguments args) throws Exception {
        Student student1 = Student.builder()
                .id(1l)
                .studentId("SE-001")
                .name("Prayuth")
                .surname("The minister")
                .gpa(3.59)
                .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/tu.jpg?alt=media&token=f16b5c1c-fbea-4d98-9fa3-732b69a1bae8")
                .penAmount(15)
                .description("The great man ever!!!!")
                .build();
        Student student2 = Student.builder()
                .id(2l)
                .studentId("SE-002")
                .name("Cherprang ")
                .surname("BNK48")
                .gpa(4.01)
                .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/cherprang.png?alt=media&token=2e6a41f3-3bf0-4e42-ac6f-8b7516e24d92")
                .penAmount(2)
                .description("Code for Thailand")
                .build();
        Student student3 = Student.builder()
                .id(3l)
                .studentId("SE-003")
                .name("Nobi")
                .surname("Nobita")
                .gpa(1.77)
                .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/nobita.jpg?alt=media&token=16e30fb0-9904-470f-b868-c35601df8326")
                .penAmount(0)
                .description("Welcome to Olympic")
                .build();
        //Save students
        studentRepository.save(student1);
        studentRepository.save(student2);
        studentRepository.save(student3);

        Lecturer lecturer1 = Lecturer.builder()
                .name("Chartchai")
                .surname("Doungsa-ard")
                .build();
        Lecturer lecturer2 = Lecturer.builder()
                .name("Jayakrit")
                .surname("Hirisajja")
                .build();
        //Save lecturers
        this.lecturerRepository.save(lecturer1);
        this.lecturerRepository.save(lecturer2);

        Course course1 = Course.builder()
                .courseId("953331")
                .courseName("Component Based Software Dev")
                .content("Nothing just for fun")
                .build();
        Course course2 = Course.builder()
                .courseId("953xxx")
                .courseName("X Project")
                .content("Do not know what to study")
                .build();
        //Save courses
        this.courseRepository.save(course1);
        this.courseRepository.save(course2);

        lecturer1.getAdvisees().add(student1);
        student1.setAdvisor(lecturer1);
        lecturer1.getAdvisees().add(student2);
        student2.setAdvisor(lecturer1);
        lecturer2.getAdvisees().add(student3);
        student3.setAdvisor(lecturer2);

        student1.getEnrolledCourses().add(course1);
        course1.getStudents().add(student1);
        student1.getEnrolledCourses().add(course2);
        course2.getStudents().add(student2);
        student2.getEnrolledCourses().add(course1);
        course1.getStudents().add(student2);
        student3.getEnrolledCourses().add(course2);
        course2.getStudents().add(student3);
        course1.setLecturer(lecturer2);
        lecturer2.getCourses().add(course1);
        course2.setLecturer(lecturer1);
        lecturer1.getCourses().add(course2);
        //Update students
        studentRepository.save(student1) ;
        studentRepository.save(student2) ;
        studentRepository.save(student3) ;

        //Extra

        Student student4 = Student.builder()
                //.id(4l)
                .studentId("SE-010")
                .name("Koy")
                .surname("Ruchawin")
                .gpa(2.01)
                .image("https://yt3.ggpht.com/a/AGF-l7_6r_NR0iZWw6Tm_eDIiYy35mIUoPqKPUCfgw=s900-c-k-c0xffffffff-no-rj-mo")
                .penAmount(1)
                .description("GET THE FUCK OUT FROM MY KITCHEN")
                .build();
        Student student5 = Student.builder()
                //.id(5l)
                .studentId("SE-011")
                .name("Nui")
                .surname("Amphon")
                .gpa(3.52)
                .image("https://img.cinemablend.com/filter:scale/quill/6/2/8/b/6/e/628b6ef0dc7b5e43caf6af0a09ad1ccbdd43f7a0.jpg?mw=600")
                .penAmount(2)
                .description("We live in a society")
                .build();
        Student student6 = Student.builder()
                //.id(6l)
                .studentId("SE-012")
                .name("O")
                .surname("Kung")
                .gpa(1.49)
                .image("https://notebookspec.com/web/wp-content/uploads/2019/06/WCCFcyberpunk20779.jpg")
                .penAmount(3)
                .description("We have a city to burn")
                .build();
        //Save students
        studentRepository.save(student4);
        studentRepository.save(student5);
        studentRepository.save(student6);

        Course course3 = Course.builder()
                .courseId("001001")
                .courseName("English for Everyday Life")
                .content("No Content")
                .build();
        Course course4 = Course.builder()
                .courseId("953100")
                .courseName("Learning Through Activity")
                .content("Learn Everythings")
                .build();
        //Save courses
        this.courseRepository.save(course3);
        this.courseRepository.save(course4);

        Lecturer lecturer3 = Lecturer.builder()
                .name("Kong")
                .surname("Kasit")
                .build();
        //Save lecturers
        this.lecturerRepository.save(lecturer3);

        lecturer3.getAdvisees().add(student4);
        student4.setAdvisor(lecturer3);
        lecturer3.getAdvisees().add(student5);
        student5.setAdvisor(lecturer3);
        lecturer3.getAdvisees().add(student6);
        student6.setAdvisor(lecturer3);

        course3.setLecturer(lecturer1);
        lecturer1.getCourses().add(course3);
        course4.setLecturer(lecturer3);
        lecturer3.getCourses().add(course4);

        student4.getEnrolledCourses().add(course4);
        course4.getStudents().add(student4);
        student5.getEnrolledCourses().add(course4);
        course4.getStudents().add(student5);
        student6.getEnrolledCourses().add(course4);
        course4.getStudents().add(student6);

        student4.getEnrolledCourses().add(course2);
        course2.getStudents().add(student4);
        student5.getEnrolledCourses().add(course2);
        course2.getStudents().add(student5);
        student6.getEnrolledCourses().add(course2);
        course2.getStudents().add(student6);

        //Update students
        studentRepository.save(student4) ;
        studentRepository.save(student5) ;
        studentRepository.save(student6) ;



    }
        private void addExtraData() {





        }


}

