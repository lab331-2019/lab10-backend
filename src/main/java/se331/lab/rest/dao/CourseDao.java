package se331.lab.rest.dao;

import se331.lab.rest.entity.Course;
import se331.lab.rest.entity.Lecturer;

import java.util.List;

public interface CourseDao {
    List<Course> getAllCourse();
    Course findById(Long courseId);
    Course saveCourse(Course course);
}
