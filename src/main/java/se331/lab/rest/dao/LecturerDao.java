package se331.lab.rest.dao;

import se331.lab.rest.entity.Lecturer;
import se331.lab.rest.entity.Student;

import java.util.List;

public interface LecturerDao {
    List<Lecturer> getAllLecturer();
    Lecturer findById(Long lecturerId);
    Lecturer saveLecturer(Lecturer lecturer);
}
