package se331.lab.rest.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.LecturerDao;
import se331.lab.rest.entity.Lecturer;

import java.util.List;
@Service
@Slf4j
public class LecturerServiceImpl implements LecturerService {
    @Autowired
    LecturerDao lecturerDao;

    @Override
    public List<Lecturer> getAllLecturer() {
        log.info("service received called");
        List<Lecturer> lecturers = lecturerDao.getAllLecturer();
        log.info("service received {} \n from dao", lecturers);
        return lecturers;
    }

    @Override
    public Lecturer findById(Long lecturerId) {
        return lecturerDao.findById(lecturerId);
    }

    @Override
    public Lecturer saveLecturer(Lecturer lecturer) {
        return lecturerDao.saveLecturer(lecturer);
    }
}
